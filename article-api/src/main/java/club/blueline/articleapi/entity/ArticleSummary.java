package club.blueline.articleapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticleSummary {

    private Integer id;
    private String title;
    private Integer userId;
    private String username;
    private Integer categoryId;
    private String categoryName;

}
