package club.blueline.articleapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticleInfoDo {

    private Integer id;
    private String title;
    private String coverUrl;
    private Integer userId;
    private Integer categoryId;
    private Timestamp time;
    private String content;

    public ArticleInfoDo(PostArticleDto postArticleDto) {
        this.title = postArticleDto.getTitle();
        this.coverUrl = postArticleDto.getCoverUrl();
        this.userId = postArticleDto.getUserId();
        this.categoryId = postArticleDto.getCategoryId();
        this.content = postArticleDto.getContent();
    }

    public ArticleInfoDo(EditArticleDto editArticleDto) {
        this.id = editArticleDto.getId();
        this.title = editArticleDto.getTitle();
        this.coverUrl = editArticleDto.getCoverUrl();
        this.categoryId = editArticleDto.getCategoryId();
        this.content = editArticleDto.getContent();
    }
}
