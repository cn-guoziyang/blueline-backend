package club.blueline.articleapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticleDetailsDto {

    private Integer id;
    private String title;
    private String coverUrl;
    private Integer userId;
    private Integer categoryId;
    private String categoryName;
    private Timestamp time;
    private String content;

    public ArticleDetailsDto(ArticleInfoDo articleInfoDo) {
        this.id = articleInfoDo.getId();
        this.title = articleInfoDo.getTitle();
        this.coverUrl = articleInfoDo.getCoverUrl();
        this.userId = articleInfoDo.getUserId();
        this.categoryId = articleInfoDo.getCategoryId();
        this.time = articleInfoDo.getTime();
        this.content = articleInfoDo.getContent();
    }

}
