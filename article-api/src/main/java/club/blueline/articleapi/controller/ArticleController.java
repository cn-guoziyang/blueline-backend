package club.blueline.articleapi.controller;

import club.blueline.articleapi.entity.*;
import club.blueline.articleapi.mapper.ArticleMapper;
import club.blueline.articleapi.utils.ResultUtils;
import com.github.pagehelper.PageHelper;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/article")
public class ArticleController {

    @Resource
    private ArticleMapper articleMapper;

    @GetMapping
    public Result getArticleDetailsById(@RequestParam(value = "id") Integer id) {
        ArticleInfoDo articleInfoDo = articleMapper.getById(id);
        if(articleInfoDo == null) {
            Result result = ResultUtils.genFailResult("无法查询到对应ID的文章！");
            result.setCode(ResultCode.NOT_FOUND.code());
            return result;
        }
        ArticleDetailsDto articleDetailsDto = new ArticleDetailsDto(articleInfoDo);
        CategoryInfoDo categoryInfoDo = articleMapper.getCategoryById(articleInfoDo.getCategoryId());
        articleDetailsDto.setCategoryName(categoryInfoDo.getName());
        return ResultUtils.genSuccessResult(articleDetailsDto);
    }

    @PostMapping
    public Result postArticle(@RequestBody PostArticleDto postArticleDto) {
        ArticleInfoDo articleInfoDo = new ArticleInfoDo(postArticleDto);
        articleInfoDo.setTime(new Timestamp(new Date().getTime()));
        if(articleMapper.addArticle(articleInfoDo) != 1) {
            Result result = ResultUtils.genFailResult("添加文章失败！");
            result.setCode(ResultCode.FAIL.code());
            return result;
        }
        return ResultUtils.genSuccessResult();
    }

    @PutMapping
    public Result editArticle(@RequestBody EditArticleDto editArticleDto) {
        ArticleInfoDo articleInfoDo = new ArticleInfoDo(editArticleDto);
        if(articleMapper.editArticleInfo(articleInfoDo) != 1) {
            Result result = ResultUtils.genFailResult("修改文章失败！");
            result.setCode(ResultCode.FAIL.code());
            return result;
        }
        return ResultUtils.genSuccessResult();
    }

    @DeleteMapping
    public Result deleteArticle(@RequestParam(value = "id") Integer id) {
        if(articleMapper.deleteArticleById(id) != 1) {
            Result result = ResultUtils.genFailResult("删除文章失败！");
            result.setCode(ResultCode.FAIL.code());
            return result;
        }
        return ResultUtils.genSuccessResult();
    }

    @GetMapping(value = "/list")
    public Result getArticleSummaryList(@RequestParam(value = "currentPage") Integer currentPage,@RequestParam(value = "pageSize") Integer pageSize) {
        PageHelper.startPage(currentPage, pageSize);
        List<ArticleSummary> summaryList = articleMapper.getArticleSummaryList();
        if(summaryList == null) {
            Result result = ResultUtils.genFailResult("获取文章列表失败！");
            result.setCode(ResultCode.FAIL.code());
            return result;
        }
        return ResultUtils.genSuccessResult(summaryList);
    }

}
