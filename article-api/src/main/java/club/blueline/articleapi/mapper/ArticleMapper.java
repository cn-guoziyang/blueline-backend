package club.blueline.articleapi.mapper;

import club.blueline.articleapi.entity.ArticleInfoDo;
import club.blueline.articleapi.entity.ArticleSummary;
import club.blueline.articleapi.entity.CategoryInfoDo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ArticleMapper {

    @Select("SELECT * FROM articleInfo WHERE id=#{id}")
    ArticleInfoDo getById(Integer id);

    @Select("SELECT * FROM categoryInfo WHERE id=#{id}")
    CategoryInfoDo getCategoryById(Integer id);

    @Insert("INSERT INTO articleInfo(title,cover_url,user_id,category_id,time,content) VALUES(#{title},#{coverUrl},#{userId},#{categoryId},#{time},#{content})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    Integer addArticle(ArticleInfoDo articleInfoDo);

    @Update("UPDATE articleInfo SET title=#{title},cover_url=#{coverUrl},category_id=#{categoryId},content=#{content} WHERE id=#{id}")
    Integer editArticleInfo(ArticleInfoDo articleInfoDo);

    @Delete("DELETE FROM articleInfo WHERE id=#{id}")
    Integer deleteArticleById(Integer id);

    @Select("SELECT a.id as id, a.title as title, a.user_id as userId, u.username as username, a.category_id as categoryId, c.name as categoryName FROM articleInfo as a,userInfo as u,categoryInfo as c WHERE a.user_id=u.id AND a.category_id=c.id")
    List<ArticleSummary> getArticleSummaryList();

}
