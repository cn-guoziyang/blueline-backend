package club.blueline.articleapi.mapper;

import club.blueline.articleapi.entity.CategoryInfoDo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CategoryMapper {

    @Select("SELECT * FROM categoryInfo")
    List<CategoryInfoDo> getAll();

    @Insert("INSERT INTO categoryInfo(name) VALUES(#{name})")
    Integer addCategory(String name);

    @Update("UPDATE categoryInfo SET name=#{name} WHERE id=#{id}")
    Integer editCategory(CategoryInfoDo categoryInfoDo);

    @Delete("DELETE FROM categoryInfo WHERE id=#{id}")
    Integer deleteCategory(Integer id);
}
