package club.blueline.consumer.service;

import club.blueline.consumer.entity.EditUserDetailDto;
import club.blueline.consumer.entity.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "user-api", path = "/api/user")
public interface UserService {

    @GetMapping
    Result getUsernameById(@RequestParam(value = "id") Integer id);

    @GetMapping("/detail")
    Result getUserDetailById(@RequestParam(value = "id") Integer id);

    @PutMapping
    Result editUserDetail(@RequestBody EditUserDetailDto userDetailDto);

    @DeleteMapping
    Result deleteUser(@RequestParam(value = "id") Integer id);

    @PostMapping("/changePass")
    Result changePassword(@RequestParam(value = "id") Integer id, @RequestParam(value = "password") String password);

    @GetMapping("/list")
    Result listUserInfo();

}
