package club.blueline.consumer.service;

import club.blueline.consumer.entity.EditArticleDto;
import club.blueline.consumer.entity.PostArticleDto;
import club.blueline.consumer.entity.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "article-api", path="/api/article")
public interface ArticleService {

    @GetMapping
    Result getArticleDetailsById(@RequestParam(value = "id") Integer id);

    @PostMapping
    Result postArticle(@RequestBody PostArticleDto postArticleDto);

    @PutMapping
    Result editArticle(@RequestBody EditArticleDto editArticleDto);

    @DeleteMapping
    Result deleteArticle(@RequestParam(value = "id") Integer id);

    @GetMapping(value = "/list")
    Result getArticleSummaryList(@RequestParam("currentPage") Integer currentPage, @RequestParam("pageSize") Integer pageSize);

}
