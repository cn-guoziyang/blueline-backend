package club.blueline.consumer.service;

import club.blueline.consumer.entity.PostRecruitDto;
import club.blueline.consumer.entity.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "user-api", path = "/api/recruit")
public interface RecruitService {

    @PostMapping
    Result postRecruit(@RequestBody PostRecruitDto postRecruitDto);

    @GetMapping("/accept")
    Result acceptRecruit(@RequestParam(value = "id") Integer id);

    @GetMapping
    Result getRecruitDetail(@RequestParam(value = "id") Integer id);

    @GetMapping("/list")
    Result listRecruits();

}
