package club.blueline.consumer.service;

import club.blueline.consumer.entity.EditCommentDto;
import club.blueline.consumer.entity.PostCommentDto;
import club.blueline.consumer.entity.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "comment-api", path = "/api/comment")
public interface CommentService {

    @GetMapping
    Result getComment(@RequestParam("articleId") Integer articleId, @RequestParam("userId") Integer userId);

    @PostMapping
    Result postComment(@RequestBody PostCommentDto dto);

    @PutMapping
    Result editComment(@RequestBody EditCommentDto dto);

    @DeleteMapping
    Result deleteComment(@RequestParam(value = "id") Integer id);

}
