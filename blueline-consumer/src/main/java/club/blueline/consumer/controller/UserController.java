package club.blueline.consumer.controller;

import club.blueline.consumer.entity.Result;
import club.blueline.consumer.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    @GetMapping
    public Result getUserDetailById(@RequestParam(value = "id") Integer id) {
        userService.getUserDetailById(id);
        return null;
    }

}
