package club.blueline.consumer.controller;

import club.blueline.consumer.entity.*;
import club.blueline.consumer.mapper.SimpleMapper;
import club.blueline.consumer.service.ArticleService;
import club.blueline.consumer.service.CommentService;
import club.blueline.consumer.utils.ResultUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/article")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @Autowired
    private CommentService commentService;

    @Resource
    private SimpleMapper simpleMapper;

    @GetMapping("/list")
    public Result getArticleList(@RequestParam(value = "currentPage", required=false) Integer currentPage, @RequestParam(value = "pageSize", required=false) Integer pageSize) {
        if(currentPage == null) currentPage = 1;
        if(pageSize == null) pageSize = 10;
        Result res = articleService.getArticleSummaryList(currentPage, pageSize);
        return res;
    }

    @GetMapping
    public Result getArticleById(@RequestParam(value = "id", required=false) Integer id) {
        if(id == null) {
            Result res = ResultUtils.genFailResult("缺少参数id");
            res.setCode(ResultCode.FAIL.code());
            return res;
        }
        Result articleRes = articleService.getArticleDetailsById(id);
        if(articleRes.getCode() != ResultCode.SUCCESS.code()) {
            return articleRes;
        }
        ArticleDetailsDto articleDetailsDto = (ArticleDetailsDto) articleRes.getData();
        ArticleDetailsVo articleVo = new ArticleDetailsVo();
        articleVo.setArticle(articleDetailsDto);
        Result commentRes = commentService.getComment(id, null);
        if(commentRes.getCode() != ResultCode.SUCCESS.code()) {
            articleVo.setComments(null);
        } else {
            articleVo.setComments((List<CommentByArticleDto>) commentRes.getData());
        }
        return ResultUtils.genSuccessResult(articleVo);
    }

    @PostMapping
    public Result postArticle(@RequestBody PostArticleDto postArticleVo) {
        return articleService.postArticle(postArticleVo);
    }

    @PutMapping
    public Result editArticle(@RequestBody EditArticleDto editArticleVo, HttpServletRequest request) {
        String idStr = request.getHeader("Authorization-id");
        String role = request.getHeader("Authorization-role");
        if(role.equals("ROLE_ADMIN") || idStr.equals(String.valueOf(simpleMapper.getUserIdByArticleId(editArticleVo.getId()))))
        return articleService.editArticle(editArticleVo);
        else {
            Result res = ResultUtils.genFailResult("未授权行为！");
            res.setCode(ResultCode.UNAUTHORIZED.code());
            return res;
        }
    }

    @DeleteMapping
    public Result deleteArticle(@RequestParam(value = "id", required=false) Integer id, HttpServletRequest request) {
        if(id == null) {
            Result res = ResultUtils.genFailResult("缺少参数id");
            res.setCode(ResultCode.FAIL.code());
            return res;
        }
        String idStr = request.getHeader("Authorization-id");
        String role = request.getHeader("Authorization-role");
        if(role.equals("ROLE_ADMIN") || idStr.equals(String.valueOf(simpleMapper.getUserIdByArticleId(id))))
            return articleService.deleteArticle(id);
        else {
            Result res = ResultUtils.genFailResult("未授权行为！");
            res.setCode(ResultCode.UNAUTHORIZED.code());
            return res;
        }
    }

}
