package club.blueline.consumer.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface SimpleMapper {

    @Select("SELECT user_id FROM articleInfo WHERE id=#{articleId}")
    Integer getUserIdByArticleId(Integer articleId);

    @Select("SELECT user_id FROM commentInfo WHERE id=#{commentId}")
    Integer getUserIdByCommentId(Integer commentId);

}
