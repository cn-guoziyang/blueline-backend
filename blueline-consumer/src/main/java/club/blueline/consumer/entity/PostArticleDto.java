package club.blueline.consumer.entity;

import lombok.Data;

@Data
public class PostArticleDto {

    private String title;
    private String coverUrl;
    private Integer userId;
    private Integer categoryId;
    private String content;

}
