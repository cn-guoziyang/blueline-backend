package club.blueline.consumer.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticleDetailsVo {

    private ArticleDetailsDto article;
    private List<CommentByArticleDto> comments;

}
