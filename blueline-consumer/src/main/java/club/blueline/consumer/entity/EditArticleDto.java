package club.blueline.consumer.entity;

import lombok.Data;

@Data
public class EditArticleDto {

    private Integer id;
    private String title;
    private String coverUrl;
    private Integer categoryId;
    private String content;

}
