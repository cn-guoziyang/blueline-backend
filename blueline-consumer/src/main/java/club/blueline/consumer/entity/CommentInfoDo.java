package club.blueline.consumer.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentInfoDo {

    private Integer id;
    private Integer articleId;
    private Integer userId;
    private String username;
    private Integer parent;
    private Timestamp time;
    private String content;

    public CommentInfoDo(PostCommentDto dto) {
        this.articleId = dto.getArticleId();
        this.userId = dto.getUserId();
        this.parent = dto.getParent();
        this.content = dto.getContent();
    }
}
