package club.blueline.commentapi.controller;

import club.blueline.commentapi.entity.*;
import club.blueline.commentapi.mapper.CommentMapper;
import club.blueline.commentapi.utils.ResultUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api/comment")
public class CommentController {

    @Resource
    private CommentMapper commentMapper;

    @GetMapping
    public Result getComment(@RequestParam(value = "articleId") Integer articleId, @RequestParam(value = "userId") Integer userId) {
        return articleId==null?getCommentByUserId(userId):getCommentByArticleId(articleId);
    }

    @PostMapping
    public Result postComment(@RequestBody PostCommentDto dto) {
        CommentInfoDo commentInfoDo = new CommentInfoDo(dto);
        commentInfoDo.setTime(new Timestamp(new Date().getTime()));
        if(commentMapper.addComment(commentInfoDo) != 1) {
            Result res = ResultUtils.genFailResult("添加评论失败！");
            res.setCode(ResultCode.FAIL.code());
            return res;
        }
        return ResultUtils.genSuccessResult();
    }

    @PutMapping
    public Result editComment(@RequestBody EditCommentDto dto) {
        if(commentMapper.updateById(dto.getId(), dto.getContent()) != 1) {
            Result res = ResultUtils.genFailResult("修改评论失败！");
            res.setCode(ResultCode.FAIL.code());
            return res;
        }
        return ResultUtils.genSuccessResult();
    }

    @DeleteMapping
    public Result deleteComment(@RequestParam(value = "id") Integer id) {
        if(commentMapper.deleteById(id) != 1) {
            Result res = ResultUtils.genFailResult("删除评论失败！");
            res.setCode(ResultCode.FAIL.code());
            return res;
        }
        return ResultUtils.genSuccessResult();
    }

    public Result getCommentByArticleId(Integer articleId) {
        List<CommentInfoDo> list = commentMapper.getByArticleId(articleId);
        if(list == null) {
            Result res = ResultUtils.genFailResult("查询出错！");
            res.setCode(ResultCode.FAIL.code());
            return res;
        }
        List<CommentByArticleDto> res = new ArrayList<>();
        List<CommentByArticleDto> tmp = new ArrayList<>();
        HashMap<Integer, CommentByArticleDto> idDto = new HashMap<>();
        for(int i = 0; i < list.size(); i ++) {
            CommentInfoDo cdo = list.get(i);
            CommentByArticleDto dto = new CommentByArticleDto(cdo);
            tmp.add(dto);
            idDto.put(dto.getId(), dto);
        }
        for(int i = 0; i < tmp.size(); i ++) {
            CommentByArticleDto dto = tmp.get(i);
            if(dto.getParent() == null) res.add(dto);
            else idDto.get(dto.getParent()).getChildren().add(dto);
        }
        return ResultUtils.genSuccessResult(res);
    }

    public Result getCommentByUserId(Integer userId) {
        List<CommentInfoDo> list = commentMapper.getByUserId(userId);
        if(list == null) {
            Result res = ResultUtils.genFailResult("查询出错！");
            res.setCode(ResultCode.FAIL.code());
            return res;
        }
        return ResultUtils.genSuccessResult(list);
    }

}
