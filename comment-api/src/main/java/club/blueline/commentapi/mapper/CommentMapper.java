package club.blueline.commentapi.mapper;

import club.blueline.commentapi.entity.CommentInfoDo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CommentMapper {

    @Select("SELECT c.*, u.username FROM commentInfo as c, userInfo as u WHERE c.user_id=u.id AND c.article_id=#{articleId}")
    List<CommentInfoDo> getByArticleId(Integer articleId);

    @Select("SELECT * FROM commentInfo WHERE user_id=#{userId}")
    List<CommentInfoDo> getByUserId(Integer userId);

    @Insert("INSERT INFO commentInfo VALUES(#{id},#{articleId},#{userId},#{parent},#{time},#{content})")
    Integer addComment(CommentInfoDo infoDo);

    @Update("UPDATE commentInfo SET content=#{content} WHERE id=#{id}")
    Integer updateById(Integer id, String content);

    @Delete("DELETE FROM commentInfo WHERE id=#{id}")
    Integer deleteById(Integer id);

}
