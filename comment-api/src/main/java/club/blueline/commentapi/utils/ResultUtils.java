package club.blueline.commentapi.utils;


import club.blueline.commentapi.entity.Result;
import club.blueline.commentapi.entity.ResultCode;

/**
 * 创建通用返回结果的工具类
 *
 * @author ziyang
 */
public class ResultUtils {

    private static final String DEFAULT_SUCCESS_MESSAGE = "SUCCESS";

    public static Result genSuccessResult() {
        return new Result()
                .setCode(ResultCode.SUCCESS.code())
                .setMessage(DEFAULT_SUCCESS_MESSAGE);
    }

    public static <T> Result<T> genSuccessResult(T data) {
        return new Result()
                .setCode(ResultCode.SUCCESS.code())
                .setMessage(DEFAULT_SUCCESS_MESSAGE)
                .setData(data);
    }

    public static Result genFailResult(String message) {
        return new Result()
                .setCode(ResultCode.FAIL.code())
                .setMessage(message);
    }
}
