package club.blueline.commentapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostCommentDto {

    private Integer articleId;
    private Integer userId;
    private Integer parent;
    private String content;

}
