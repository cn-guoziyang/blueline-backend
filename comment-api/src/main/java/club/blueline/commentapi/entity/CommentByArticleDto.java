package club.blueline.commentapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentByArticleDto {

    private Integer id;
    private Integer userId;
    private String username;
    private Integer parent;
    private Timestamp time;
    private String content;
    private List<CommentByArticleDto> children;

    public CommentByArticleDto(CommentInfoDo cdo) {
        this.id = cdo.getId();
        this.userId = cdo.getUserId();
        this.username = cdo.getUsername();
        this.parent = cdo.getParent();
        this.time = cdo.getTime();
        this.content = cdo.getContent();
        this.children = new ArrayList<>();
    }

}
