package club.blueline.userapi.controller;

import club.blueline.userapi.entity.*;
import club.blueline.userapi.mapper.RecruitMapper;
import club.blueline.userapi.mapper.UserMapper;
import club.blueline.userapi.utils.ResultUtils;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/api/recruit")
public class RecruitController {

    @Resource
    private UserMapper userMapper;
    @Resource
    private RecruitMapper recruitMapper;
    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @PostMapping
    @Transactional(propagation = Propagation.REQUIRED)
    public Result postRecruit(@RequestBody PostRecruitDto postRecruitDto) {
        RecruitInfoDo recruitInfoDo = new RecruitInfoDo(postRecruitDto);
        if(recruitMapper.saveRecruit(recruitInfoDo) != 1) {
            return ResultUtils.genFailResult("申请失败！").setCode(ResultCode.FAIL.code());
        }
        return ResultUtils.genSuccessResult();
    }

    @GetMapping("/accept")
    @Transactional(propagation = Propagation.REQUIRED)
    public Result acceptRecruit(@RequestParam(value = "id") Integer id) throws BadHanyuPinyinOutputFormatCombination {
        if(recruitMapper.setAdmitted(id) != 1) {
            return ResultUtils.genFailResult("无此ID的申请！").setCode(ResultCode.FAIL.code());
        }
        RecruitInfoDo recruitInfo = recruitMapper.getById(id);

        UserInfoDo userInfoDo = new UserInfoDo();
        HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
        format.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        format.setVCharType(HanyuPinyinVCharType.WITH_V);
        String username = PinyinHelper.toHanYuPinyinString(recruitInfo.getName(), format, "", true);
        username += recruitInfo.getContact().substring(recruitInfo.getContact().length()-4);
        userInfoDo.setUsername(username);
        userInfoDo.setPassword(bCryptPasswordEncoder.encode(username));
        userInfoDo.setEnable(1);
        if(userMapper.save(userInfoDo) != 1) {
            return ResultUtils.genFailResult("添加用户信息失败！").setCode(ResultCode.FAIL.code());
        }

        UserDetailDo userDetailDo = new UserDetailDo(recruitInfo);
        userDetailDo.setId(userInfoDo.getId());
        if(userMapper.saveDetail(userDetailDo) != 1) {
            return ResultUtils.genFailResult("添加用户详情失败！").setCode(ResultCode.FAIL.code());
        }
        return ResultUtils.genSuccessResult();
    }

    @GetMapping
    public Result getRecruitDetail(@RequestParam(value = "id") Integer id) {
        RecruitInfoDo recruitInfo = recruitMapper.getById(id);
        if(recruitInfo == null) {
            return ResultUtils.genFailResult("无此ID的申请！").setCode(ResultCode.FAIL.code());
        }
        return ResultUtils.genSuccessResult(recruitInfo);
    }

    @GetMapping("/list")
    public Result listRecruits() {
        List<SimpleRecruitDo> list = recruitMapper.getList();
        if(list == null) {
            return ResultUtils.genFailResult("获取失败！").setCode(ResultCode.FAIL.code());
        }
        return ResultUtils.genSuccessResult(list);
    }

}
