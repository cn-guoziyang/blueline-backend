package club.blueline.userapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class UserApiMain {

    public static void main(String[] args) {
        SpringApplication.run(UserApiMain.class, args);
    }

}
