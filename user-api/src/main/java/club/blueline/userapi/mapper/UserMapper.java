package club.blueline.userapi.mapper;

import club.blueline.userapi.entity.EditUserDetailDto;
import club.blueline.userapi.entity.UserDetailDo;
import club.blueline.userapi.entity.UserDetailDto;
import club.blueline.userapi.entity.UserInfoDo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserMapper {

    @Select("SELECT username FROM userInfo WHERE id=#{id}")
    String getUsernameById(Integer id);

    @Select("SELECT d.*, u.username FROM userDetail as d,userInfo as u WHERE d.id=u.id AND u.id=#{id}")
    UserDetailDto getUserDetailById(Integer id);

    @Update("UPDATE userInfo SET username=#{username} WHERE id=#{id}")
    Integer editUsername(Integer id, String username);

    @Update("UPDATE userDetail SET name=#{name},contact=#{contact},email=#{email},degree=#{degree},school=#{school},admission_year=#{admissionYear},avatar=#{avatar},signature=#{signature} WHERE id=#{id}")
    Integer editUserDetail(EditUserDetailDto userDetailDto);

    @Update("UPDATE userInfo SET enable=0 WHERE id=#{id}")
    Integer deleteUser(Integer id);

    @Update("UPDATE userInfo SET password=#{password} WHERE id=#{id}")
    Integer changePassword(Integer id, String password);

    @Select("SELECT * FROM userInfo")
    List<UserInfoDo> getAll();

    @Insert("INSERT INTO userInfo VALUES(#{id}, #{username}, #{password}, #{enable})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    Integer save(UserInfoDo infoDo);

    @Insert("INSERT INFO userDetail VALUES(#{id},#{name},#{gender},#{contact},#{qq},#{email},#{degree},#{school},#{admissionYear},#{enterClubYear},#{avatar},#{signature}")
    Integer saveDetail(UserDetailDo userDetailDo);

}
