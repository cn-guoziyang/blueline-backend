package club.blueline.userapi.mapper;

import club.blueline.userapi.entity.RecruitInfoDo;
import club.blueline.userapi.entity.SimpleRecruitDo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface RecruitMapper {

    @Insert("INSERT INTO recruitInfo VALUES(#{id},#{name},#{gender},#{contact},#{qq},#{email},#{degree},#{school},#{admissionYear},#{admitted})")
    Integer saveRecruit(RecruitInfoDo infoDo);

    @Update("UPDATE recruitInfo SET admitted=1 WHERE id=#{id}")
    Integer setAdmitted(Integer id);

    @Select("SELECT * FROM recruitInfo WHERE id=#{id}")
    RecruitInfoDo getById(Integer id);

    @Select("SELECT id,name,gender,admitted FROM recruitInfo")
    List<SimpleRecruitDo> getList();

}
