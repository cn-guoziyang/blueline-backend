package club.blueline.userapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDetailDo {

    private Integer id;
    private String name;
    private Integer gender;
    private String contact;
    private String qq;
    private String email;
    private String degree;
    private String school;
    private String admissionYear;
    private String enterClubYear;
    private String avatar;
    private String signature;

    public UserDetailDo(RecruitInfoDo infoDo) {
        this.name = infoDo.getName();
        this.gender = infoDo.getGender();
        this.contact = infoDo.getContact();
        this.qq = infoDo.getQq();
        this.email = infoDo.getEmail();
        this.degree = infoDo.getDegree();
        this.school = infoDo.getSchool();
        this.admissionYear = infoDo.getAdmissionYear();
        this.enterClubYear = new SimpleDateFormat("yyyy").format(new Date());
    }

}
