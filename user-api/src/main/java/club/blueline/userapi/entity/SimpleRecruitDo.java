package club.blueline.userapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SimpleRecruitDo {

    private Integer id;
    private String name;
    private Integer gender;
    private Integer admitted;

}
