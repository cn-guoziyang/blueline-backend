package club.blueline.userapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostRecruitDto {

    private String name;
    private Integer gender;
    private String contact;
    private String qq;
    private String email;
    private String degree;
    private String school;
    private String admissionYear;

}
