package club.blueline.userapi.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDetailDto {

    private Integer id;
    private String username;
    private String name;
    private String contact;
    private String email;
    private String degree;
    private String school;
    private String admissionYear;
    private String enterClubYear;
    private String avatar;
    private String signature;
    private Integer admitted;

}
