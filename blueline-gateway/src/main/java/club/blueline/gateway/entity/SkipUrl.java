package club.blueline.gateway.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SkipUrl {

    private String method;
    private String url;

    @Override
    public int hashCode() {
        StringBuilder builder = new StringBuilder();
        builder.append(method);
        builder.append(url);
        char[] chars = builder.toString().toCharArray();
        int hash = 0;
        for(int i = 0; i < chars.length; i ++) {
            hash = hash * 131 + chars[i];
        }
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(!(obj instanceof SkipUrl)) return false;
        SkipUrl ins = (SkipUrl) obj;
        if(!this.method.equals(ins.method)) {
            return false;
        }
        if(!this.url.equals(ins.url)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{method: " + method + ", url: " + url + "}";
    }

}
