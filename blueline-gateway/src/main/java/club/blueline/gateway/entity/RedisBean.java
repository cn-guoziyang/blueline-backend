package club.blueline.gateway.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RedisBean {

    private Integer id;
    private String token;
    private String role;
    private Set<ApiAuthority> authorities;

}
