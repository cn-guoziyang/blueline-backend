package club.blueline.auth.mapper;

import club.blueline.auth.entity.ApiAuthority;
import club.blueline.auth.entity.UserRoleDo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
@Mapper
public interface UserRoleMapper {

    @Insert("INSERT INTO userRole VALUES(#{id},#{userId},#{roleId})")
    Integer save(UserRoleDo userRoleDo);

    @Select("SELECT a.api_method as method, a.api_url as url FROM apiInfo as a, roleApi as ra, roleInfo as r WHERE r.role_name = #{roleName} AND ra.role_id = r.id AND ra.api_id = a.id")
    Set<ApiAuthority> getAuthoritiesByRoleName(String roleName);

}
