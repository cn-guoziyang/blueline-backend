package club.blueline.auth.mapper;

import club.blueline.auth.entity.UserDetailsDo;
import club.blueline.auth.entity.UserInfoDo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface UserInfoMapper {

    @Insert("INSERT INTO userInfo VALUES(#{id}, #{username},#{password},#{enable})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    Integer save(UserInfoDo user);

    @Select("SELECT u.id as id, u.username as username, u.password as password, u.enable as enable, r.role_name as role FROM userInfo as u, roleInfo as r, userRole as ur WHERE u.username=#{username} AND ur.user_id=u.id AND ur.role_id=r.id")
    UserDetailsDo findUserDetailsByUserName(String username);
}
