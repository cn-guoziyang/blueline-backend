package club.blueline.auth.service;

import club.blueline.auth.entity.RegisterUserVo;

public interface UserService {

    boolean registerUser(RegisterUserVo userVo);

}
