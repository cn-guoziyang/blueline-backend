package club.blueline.auth.service.impl;

import club.blueline.auth.entity.RegisterUserVo;
import club.blueline.auth.entity.UserInfoDo;
import club.blueline.auth.entity.UserRoleDo;
import club.blueline.auth.mapper.UserInfoMapper;
import club.blueline.auth.mapper.UserRoleMapper;
import club.blueline.auth.service.UserService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Resource
    private UserInfoMapper userInfoMapper;
    @Resource
    private UserRoleMapper userRoleMapper;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean registerUser(RegisterUserVo userVo) {
        UserInfoDo userInfoDo = new UserInfoDo(null, userVo.getUsername(), bCryptPasswordEncoder.encode(userVo.getPassword()), 1);
        if(userInfoMapper.save(userInfoDo) != 1) {
            return false;
        }
        UserRoleDo userRoleDo =new UserRoleDo(null, userInfoDo.getId(), 1);
        return userRoleMapper.save(userRoleDo) == 1;
    }

}
