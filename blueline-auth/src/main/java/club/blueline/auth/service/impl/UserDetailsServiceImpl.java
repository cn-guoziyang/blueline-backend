package club.blueline.auth.service.impl;

import club.blueline.auth.entity.ApiAuthority;
import club.blueline.auth.entity.JwtUser;
import club.blueline.auth.entity.UserDetailsDo;
import club.blueline.auth.mapper.UserInfoMapper;
import club.blueline.auth.mapper.UserRoleMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Set;

@Service("userDetailsServiceImpl")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Resource
    private UserInfoMapper userInfoMapper;
    @Resource
    private UserRoleMapper userRoleMapper;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserDetailsDo userDetailsDo = userInfoMapper.findUserDetailsByUserName(s);
        Set<ApiAuthority> authorities = userRoleMapper.getAuthoritiesByRoleName(userDetailsDo.getRole());
        JwtUser jwtUser = new JwtUser(userDetailsDo);
        jwtUser.setApiAuthorities(authorities);
        return jwtUser;
    }
}
