package club.blueline.auth.controller;

import club.blueline.auth.entity.RegisterUserVo;
import club.blueline.auth.entity.Result;
import club.blueline.auth.entity.ResultCode;
import club.blueline.auth.service.UserService;
import club.blueline.auth.utils.ResultUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Resource
    private UserService userService;

    @PostMapping("/register")
    public Result registerUser(@RequestBody RegisterUserVo userVo) {
        if(userService.registerUser(userVo)) {
            return ResultUtils.genSuccessResult();
        } else {
            Result result = ResultUtils.genFailResult("注册失败！");
            result.setCode(ResultCode.FAIL.code());
            return result;
        }
    }

}
