package club.blueline.auth.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoDo {

    private Integer id;
    private String username;
    private String password;
    private int enable;

}
