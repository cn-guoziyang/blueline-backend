package club.blueline.auth.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginData {

    private Integer id;
    private String username;
    private String role;

}
