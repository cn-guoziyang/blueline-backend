package club.blueline.auth.entity;

import lombok.Data;

@Data
public class UserDetailsDo {

    private Integer id;
    private String username;
    private String password;
    private String role;
    private Integer enable;

}
