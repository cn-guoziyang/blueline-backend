package club.blueline.auth.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

public class JwtUser implements UserDetails {

    private Integer id;
    private String username;
    private String password;
    private Boolean enable;
    private Set<ApiAuthority> apiAuthorities;
    private Collection<? extends GrantedAuthority> authorities;

    public JwtUser() {}

    public JwtUser(UserDetailsDo user) {
        id = user.getId();
        username = user.getUsername();
        password = user.getPassword();
        enable = user.getEnable()==1;
        authorities = Collections.singleton(new SimpleGrantedAuthority(user.getRole()));
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public Integer getId() {
        return id;
    }

    public void setApiAuthorities(Set<ApiAuthority> apiAuthorities) {
        this.apiAuthorities = apiAuthorities;
    }

    public Set<ApiAuthority> getApiAuthorities() {
        return apiAuthorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enable;
    }

    @Override
    public String toString() {
        return "id: " + id + "\nusername: " + username + "\nAuthorities: " + apiAuthorities.toString();
    }
}
