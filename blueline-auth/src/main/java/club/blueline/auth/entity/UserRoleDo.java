package club.blueline.auth.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRoleDo {

    private Integer id;
    private Integer userId;
    private Integer roleId;

}
