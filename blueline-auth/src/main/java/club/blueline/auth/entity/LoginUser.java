package club.blueline.auth.entity;

import lombok.Data;

@Data
public class LoginUser {

    private String username;
    private String password;

}
